package org.mpierce.jersey2.metrics.stockannotations;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.annotation.Timed;
import com.codahale.metrics.jersey2.InstrumentedResourceMethodApplicationListener;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.mpierce.jersey2.metrics.MetricNamer;

import javax.annotation.Nullable;
import java.lang.reflect.Method;

/**
 * Names timers for methods annotated with {@link Timed} according to the logic in {@code metrics-jersey2}'s {@link
 * InstrumentedResourceMethodApplicationListener}.
 */
public final class StockAnnotationNamer implements MetricNamer {
    @Nullable
    @Override
    public String getTimerName(RequestEvent event) {

        ResourceMethod resourceMethod = event.getContainerRequest().getUriInfo().getMatchedResourceMethod();
        Method method = resourceMethod.getInvocable().getDefinitionMethod();
        Timed annotation = method.getAnnotation(Timed.class);
        if (annotation == null) {
            return null;
        }

        return ExposeChooseName.exposeChooseName(annotation.name(), annotation.absolute(), resourceMethod);
    }

    @Nullable
    @Override
    public String getStatusCodeCounterName(RequestEvent event) {
        return null;
    }

    private static class ExposeChooseName extends InstrumentedResourceMethodApplicationListener {

        private ExposeChooseName(MetricRegistry metrics) {
            super(metrics);
        }

        static String exposeChooseName(final String explicitName, final boolean absolute, final ResourceMethod method,
            final String... suffixes) {
            return InstrumentedResourceMethodApplicationListener.chooseName(explicitName, absolute, method, suffixes);
        }
    }
}
