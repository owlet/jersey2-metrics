package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("resourceWithSubresource")
public class ResourceWithSubresource {
    @GET
    @Path("sub")
    public String get() {
        return "get";
    }
}
