package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.Path;

@Path("resourceWithSubResourceLocatorsToSameSubResource")
public class ResourceWithTwoSubresourceLocatorsToSameSubResource {
    @Path("simpleSrl")
    public ResourceWithoutPathWithSubResource getSub() {
        return new ResourceWithoutPathWithSubResource();
    }

    @Path("{pathParamAsSrl}")
    public ResourceWithoutPathWithSubResource getRes() {
        return new ResourceWithoutPathWithSubResource();
    }
}
