package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("resourceWithPathParam/{pp}")
public class BasicResourceWithPathParam {
    @GET
    public String get(@PathParam("pp") String pp) {
        return "get";
    }
}
