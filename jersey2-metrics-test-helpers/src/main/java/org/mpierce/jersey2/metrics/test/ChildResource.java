package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("child")
public final class ChildResource extends ParentResource {

    @GET
    public String get() {
        return "child";
    }
}
