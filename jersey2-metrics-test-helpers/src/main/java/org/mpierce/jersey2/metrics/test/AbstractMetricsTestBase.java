package org.mpierce.jersey2.metrics.test;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Preconditions;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import javax.servlet.Servlet;
import org.eclipse.jetty.server.NetworkConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mpierce.jersey2.metrics.MetricArbiter;
import org.mpierce.jersey2.metrics.MetricNamer;
import org.mpierce.jersey2.metrics.MetricsAppEventListener;
import org.mpierce.metrics.reservoir.hdrhistogram.HdrHistogramReservoir;
import org.slf4j.bridge.SLF4JBridgeHandler;

import static com.google.common.collect.Lists.newArrayList;

public abstract class AbstractMetricsTestBase {
    private int port;

    @BeforeClass
    public static void classSetUp() {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
    }

    private Server server;

    protected BlockingQueue<RequestEvent> eventQueue;
    protected AsyncHttpClient httpClient;
    protected MetricRegistry registry;

    @Before
    public void setUp() throws Exception {
        registry = new MetricRegistry();
        preSetUp();
        eventQueue = new ArrayBlockingQueue<>(100);
        MetricsAppEventListener listener =
                new MetricsAppEventListener.Builder(registry)
                        .withArbiter(getMetricArbiter())
                        .withNamer(getMetricNamer())
                        .withReservoirSupplier(HdrHistogramReservoir::new)
                        .build();

        ArrayList<Object> components = newArrayList(listener, new RequestEventCaptureAppEventListener(eventQueue));
        Servlet servlet = new ServletContainer(new TestJersey2App(components, getComponentClasses()));

        server = getServer(servlet);
        server.start();

        NetworkConnector connector = (NetworkConnector) server.getConnectors()[0];
        port = connector.getLocalPort();

        httpClient = new AsyncHttpClient();
        postSetUp();
    }

    protected void preSetUp() {
        // override me
    }

    protected void postSetUp() {
        // override me
    }

    protected List<Class<?>> getComponentClasses() {
        // override me
        return newArrayList();
    }

    @After
    public void tearDown() throws Exception {
        httpClient.close();

        server.stop();
    }

    protected abstract MetricArbiter getMetricArbiter();

    protected abstract MetricNamer getMetricNamer();

    protected Response req(String path) throws InterruptedException, ExecutionException {
        return httpClient.prepareGet("http://localhost:" + port + path).execute().get();
    }

    protected RequestEvent getEvent() throws InterruptedException {
        RequestEvent event = eventQueue.poll(1, TimeUnit.SECONDS);
        Preconditions.checkNotNull(event);
        return event;
    }

    private static Server getServer(Servlet servlet) {
        Server server = new Server(0);
        ServletContextHandler servletHandler = new ServletContextHandler();

        servletHandler.addServlet(new ServletHolder(servlet), "/*");

        server.setHandler(servletHandler);

        return server;
    }
}
