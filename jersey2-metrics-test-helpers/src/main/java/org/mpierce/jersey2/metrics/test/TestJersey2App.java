package org.mpierce.jersey2.metrics.test;

import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class TestJersey2App extends ResourceConfig {
    public TestJersey2App(List<Object> components, List<Class<?>> componentClasses) {
        components.forEach(this::register);

        register(BasicResource.class);
        register(ResourceWithSubresource.class);
        register(ResourceWithSubresourceLocators.class);
        register(BasicResourceWithPathParam.class);
        register(ResourceWithTwoSubresourceLocatorsToSameSubResource.class);
        register(ChildResource.class);
        register(ParentResource.class);

        componentClasses.forEach(this::register);

        final Resource.Builder resourceBuilder = Resource.builder();
        resourceBuilder.path("programmaticResource");

        final ResourceMethod.Builder methodBuilder = resourceBuilder.addMethod("GET");
        // using a lambda makes jersey sad because it can't reflectively access the type
        //noinspection Convert2Lambda
        methodBuilder.produces(MediaType.TEXT_PLAIN_TYPE)
            .handledBy(new Inflector<ContainerRequestContext, Object>() {
                @Override
                public Object apply(ContainerRequestContext containerRequestContext) {
                    return "Such dynamic. Very resource. Wow.";
                }
            });

        final Resource resource = resourceBuilder.build();
        registerResources(resource);
    }
}
