package org.mpierce.jersey2.metrics;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.junit.Test;
import org.mpierce.jersey2.metrics.test.AbstractMetricsTestBase;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;

public class RequestPathMetricNamerTest extends AbstractMetricsTestBase {

    private RequestPathMetricNamer namer;

    @Test
    public void testGetPathForPlainResourceMethod() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resource").getStatusCode());

        assertEquals("/resource", getPath(getEvent()));
    }

    @Test
    public void testGetPathForResourceWithPathParam() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resourceWithPathParam/zomg").getStatusCode());

        assertEquals("/resourceWithPathParam/{pp}", getPath(getEvent()));
    }

    @Test
    public void testGetPathForResourceWithSubresource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resourceWithSubresource/sub").getStatusCode());

        assertEquals("/resourceWithSubresource/sub", getPath(getEvent()));
    }

    @Test
    public void testGetPathForResourceWithSubresourceLocator() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resourceWithSubResourceLocators/srlWithResource").getStatusCode());

        assertEquals("/resourceWithSubResourceLocators/srlWithResource", getPath(getEvent()));
    }

    @Test
    public void testGetPathForResourceWithSubresourceLocatorToSubresource() throws ExecutionException,
        InterruptedException {
        assertEquals(200, req("/resourceWithSubResourceLocators/srlWithSubResource/subResource").getStatusCode());

        assertEquals("/resourceWithSubResourceLocators/srlWithSubResource/subResource", getPath(getEvent()));
    }

    @Test
    public void testGetPathForSameSubresourceViaTwoDifferentSubresourceLocators() throws ExecutionException,
        InterruptedException {
        assertEquals(200,
            req("/resourceWithSubResourceLocatorsToSameSubResource/simpleSrl/subResource").getStatusCode());

        assertEquals("/resourceWithSubResourceLocatorsToSameSubResource/simpleSrl/subResource", getPath(getEvent()));

        assertEquals(200, req("/resourceWithSubResourceLocatorsToSameSubResource/asdf/subResource").getStatusCode());

        assertEquals("/resourceWithSubResourceLocatorsToSameSubResource/{pathParamAsSrl}/subResource",
            getPath(getEvent()));
    }

    @Test
    public void testGetPathForVirtualSubresourceMethod() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/programmaticResource").getStatusCode());

        RequestEvent event = getEvent();

        assertEquals("/programmaticResource", getPath(event));
    }

    @Test
    public void testGetTimerName() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/programmaticResource").getStatusCode());

        RequestEvent event = getEvent();

        assertEquals("/programmaticResource.method=GET.timer", namer.getTimerName(event));
    }

    @Test
    public void testGetStatusCodeCounterName() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/programmaticResource").getStatusCode());

        RequestEvent event = getEvent();

        assertEquals("/programmaticResource.method=GET.status=200.counter",
            namer.getStatusCodeCounterName(event));
    }

    private String getPath(RequestEvent event) {
        StringBuilder buf = new StringBuilder();
        RequestPathMetricNamer.appendPath(buf, event.getUriInfo().getMatchedTemplates());
        return buf.toString();
    }

    @Override
    protected void preSetUp() {
        namer = new RequestPathMetricNamer();
    }

    @Override
    protected MetricArbiter getMetricArbiter() {
        return new FixedMetricArbiter(true, true);
    }

    @Override
    protected MetricNamer getMetricNamer() {
        return namer;
    }
}
