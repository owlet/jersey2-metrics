package org.mpierce.jersey2.metrics;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Reservoir;
import com.codahale.metrics.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

@ThreadSafe
final class MetricProvider {
    private static final Logger logger = LoggerFactory.getLogger(MetricProvider.class);

    private final MetricRegistry metricRegistry;
    private final Supplier<Reservoir> reservoirSupplier;

    private final ConcurrentMap<String, Timer> timers = new ConcurrentHashMap<>();

    MetricProvider(MetricRegistry metricRegistry, Supplier<Reservoir> reservoirSupplier) {
        this.metricRegistry = metricRegistry;
        this.reservoirSupplier = reservoirSupplier;
    }

    @Nonnull
    Timer getTimer(String timerName) {
        // no way to create a metric w/ custom reservoir only if it doesn't exist already, so we keep track of our own timer map
        return timers.computeIfAbsent(timerName, (name) -> {
            // always use default clock because we can't calculate the start right for any other time of clock.
            // See org.mpierce.jersey2.metrics.MetricsRequestEventListener.
            Timer timer = new Timer(reservoirSupplier.get());
            try {
                metricRegistry.register(name, timer);
            } catch (IllegalArgumentException e) {
                logger.warn("Registry already contains a metric for <" + name + ">. Name clash?");
                return metricRegistry.timer(name);
            }

            return timer;
        });
    }

    @Nonnull
    Counter getStatusCodeCounter(String counterName) {
        return metricRegistry.counter(counterName);
    }
}
