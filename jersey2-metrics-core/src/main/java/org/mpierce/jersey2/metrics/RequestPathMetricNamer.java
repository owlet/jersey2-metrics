package org.mpierce.jersey2.metrics;

import com.google.common.annotations.VisibleForTesting;
import java.util.List;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.uri.UriTemplate;

@ThreadSafe
public final class RequestPathMetricNamer implements MetricNamer {

    @Nullable
    @Override
    public String getTimerName(RequestEvent event) {
        StringBuilder buf = new StringBuilder(32);
        appendPath(buf, event.getUriInfo().getMatchedTemplates());

        buf.append(".method=")
            .append(event.getContainerRequest().getMethod())
            .append(".timer");
        return buf.toString();
    }

    @Nullable
    @Override
    public String getStatusCodeCounterName(RequestEvent event) {
        StringBuilder buf = new StringBuilder(32);
        appendPath(buf, event.getUriInfo().getMatchedTemplates());

        buf.append(".method=")
            .append(event.getContainerRequest().getMethod())
            .append(".status=")
            .append(event.getContainerResponse().getStatus())
            .append(".counter");

        return buf.toString();
    }

    @VisibleForTesting
    static void appendPath(StringBuilder buf, List<UriTemplate> orderedTemplates) {
        // TODO one way to avoid allocating here would be to use a trie of the template strings

        for (int i = orderedTemplates.size() - 1; i >= 0; i--) {
            UriTemplate uriTemplate = orderedTemplates.get(i);
            String s = uriTemplate.getTemplate();

            buf.append(s);
            if (uriTemplate.endsWithSlash()) {
                buf.setLength(buf.length() - 1);
            }
        }
    }
}
