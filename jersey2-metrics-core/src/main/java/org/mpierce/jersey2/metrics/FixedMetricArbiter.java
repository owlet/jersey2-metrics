package org.mpierce.jersey2.metrics;

import jdk.nashorn.internal.ir.annotations.Immutable;
import org.glassfish.jersey.server.monitoring.RequestEvent;

import javax.annotation.Nonnull;

/**
 * Simple global metric configuration.
 */
@Immutable
public final class FixedMetricArbiter implements MetricArbiter {

    private final boolean timer;
    private final boolean statusCodeCounter;

    /**
     * @param timer             enable all timers
     * @param statusCodeCounter enable all status code counters
     */
    public FixedMetricArbiter(boolean timer, boolean statusCodeCounter) {
        this.timer = timer;
        this.statusCodeCounter = statusCodeCounter;
    }

    @Override
    public boolean shouldHaveTimer(@Nonnull RequestEvent event) {
        return timer;
    }

    @Override
    public boolean shouldHaveStatusCodeCounter(@Nonnull RequestEvent event) {
        return statusCodeCounter;
    }
}
