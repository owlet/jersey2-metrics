package org.mpierce.jersey2.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Reservoir;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.mpierce.metrics.reservoir.hdrhistogram.HdrHistogramReservoir;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Supplier;

@ThreadSafe
public final class MetricsAppEventListener implements ApplicationEventListener {

    private final MetricNamer metricNamer;
    private final MetricArbiter metricArbiter;
    private final MetricProvider metricProvider;

    private MetricsAppEventListener(MetricRegistry metricRegistry, MetricNamer metricNamer, MetricArbiter metricArbiter,
        Supplier<Reservoir> reservoirSupplier) {
        this.metricNamer = metricNamer;
        this.metricArbiter = metricArbiter;
        metricProvider = new MetricProvider(metricRegistry, reservoirSupplier);
    }

    @Override
    public void onEvent(ApplicationEvent event) {
        // no op
    }

    @Override
    public RequestEventListener onRequest(RequestEvent requestEvent) {
        // Since this is of type START, we're missing information that probably any useful implementation of
        // MetricArbiter would need to be able to say that neither metric is needed. Thus, we can't return null here
        // even though an Arbiter might later decide to calculate no metrics.
        return new MetricsRequestEventListener(metricArbiter, metricProvider, metricNamer);
    }

    public static class Builder {
        private final MetricRegistry registry;

        private MetricNamer metricNamer = new RequestPathMetricNamer();
        private MetricArbiter metricArbiter = new FixedMetricArbiter(true, true);
        private Supplier<Reservoir> reservoirSupplier = HdrHistogramReservoir::new;

        public Builder(@Nonnull MetricRegistry registry) {
            this.registry = registry;
        }

        public Builder withNamer(@Nonnull MetricNamer metricNamer) {
            this.metricNamer = metricNamer;
            return this;
        }

        public Builder withArbiter(@Nonnull MetricArbiter metricArbiter) {
            this.metricArbiter = metricArbiter;
            return this;
        }

        public Builder withReservoirSupplier(Supplier<Reservoir> supplier) {
            reservoirSupplier = supplier;
            return this;
        }

        @Nonnull
        public MetricsAppEventListener build() {
            return new MetricsAppEventListener(registry, metricNamer, metricArbiter, reservoirSupplier);
        }
    }
}
