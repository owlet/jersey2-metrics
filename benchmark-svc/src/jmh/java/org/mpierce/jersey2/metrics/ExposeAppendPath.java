package org.mpierce.jersey2.metrics;

import java.util.List;
import org.glassfish.jersey.uri.UriTemplate;

public final class ExposeAppendPath {

    public static void exposeAppendPath(StringBuilder buf, List<UriTemplate> matchedTemplates) {
        RequestPathMetricNamer.appendPath(buf, matchedTemplates);
    }
}
