package org.mpierce.jersey2.metrics.jmh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import org.glassfish.jersey.uri.UriTemplate;
import org.mpierce.jersey2.metrics.ExposeAppendPath;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@BenchmarkMode(Mode.Throughput)
public class MetricNamerBenchmark {

    @State(Scope.Thread)
    public static class ThreadState {
        private StringBuilder buf = new StringBuilder();
    }

    @Benchmark
    @GroupThreads(2)
    public String generatePathInfoLinkedList(ThreadState threadState) throws Exception {
        StringBuilder buf = threadState.buf;
        buf.setLength(0);

        // this is what jersey does: linked list wrapped in unmodifiableList
        LinkedList<UriTemplate> templates = new LinkedList<>();

        templates.add(new UriTemplate("foo"));
        templates.add(new UriTemplate("bar"));
        templates.add(new UriTemplate("baz"));

        ExposeAppendPath.exposeAppendPath(buf, Collections.unmodifiableList(templates));
        return buf.toString();
    }

    @Benchmark
    @GroupThreads(2)
    public String generatePathInfoArrayList(ThreadState threadState) throws Exception {
        StringBuilder buf = threadState.buf;
        buf.setLength(0);

        ArrayList<UriTemplate> templates = new ArrayList<>();

        templates.add(new UriTemplate("foo"));
        templates.add(new UriTemplate("bar"));
        templates.add(new UriTemplate("baz"));

        ExposeAppendPath.exposeAppendPath(buf, templates);
        return buf.toString();
    }
}
