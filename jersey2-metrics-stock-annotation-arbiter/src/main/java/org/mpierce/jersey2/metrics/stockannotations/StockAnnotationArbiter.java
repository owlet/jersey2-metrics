package org.mpierce.jersey2.metrics.stockannotations;

import com.codahale.metrics.annotation.Timed;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.mpierce.jersey2.metrics.MetricArbiter;

import javax.annotation.Nonnull;

/**
 * An Arbiter that creates timers for methods annotated with {@link Timed}.
 */
public final class StockAnnotationArbiter implements MetricArbiter {
    @Override
    public boolean shouldHaveTimer(@Nonnull RequestEvent event) {
        return event
            .getContainerRequest()
            .getUriInfo()
            .getMatchedResourceMethod()
            .getInvocable()
            .getDefinitionMethod()
            .getAnnotation(Timed.class) != null;
    }

    @Override
    public boolean shouldHaveStatusCodeCounter(@Nonnull RequestEvent event) {
        return false;
    }
}
