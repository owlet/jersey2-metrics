package org.mpierce.jersey2.metrics.annotation;

import com.google.common.collect.Lists;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.junit.Test;
import org.mpierce.jersey2.metrics.FixedMetricArbiter;
import org.mpierce.jersey2.metrics.MetricArbiter;
import org.mpierce.jersey2.metrics.MetricNamer;
import org.mpierce.jersey2.metrics.test.AbstractMetricsTestBase;
import org.mpierce.jersey2.metrics.test.StubNamer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AnnotationArbiterTest extends AbstractMetricsTestBase {

    private AnnotationArbiter annotationArbiter = new AnnotationArbiter();

    @Test
    public void testRequestDefault() throws ExecutionException, InterruptedException {
        assertFalse(annotationArbiter.shouldHaveTimer(waitForOkRequest("/foo")));
    }

    @Test
    public void testRequestDisabledOnClass() throws ExecutionException, InterruptedException {
        assertFalse(annotationArbiter.shouldHaveTimer(waitForOkRequest("/fooResourceDisabled")));
    }

    @Test
    public void testRequestDisabledOnClassEnabledOnMethod() throws ExecutionException, InterruptedException {
        assertTrue(annotationArbiter.shouldHaveTimer(waitForOkRequest("/fooResourceDisabledMethodEnabled")));
    }

    @Test
    public void testRequestEnabledOnClass() throws ExecutionException, InterruptedException {
        assertTrue(annotationArbiter.shouldHaveTimer(waitForOkRequest("/fooResourceEnabled")));
    }

    @Override
    protected MetricArbiter getMetricArbiter() {
        // don't actually need to capture metrics for this test
        return new FixedMetricArbiter(false, false);
    }

    @Override
    protected MetricNamer getMetricNamer() {
        return new StubNamer();
    }

    @Override
    protected List<Class<?>> getComponentClasses() {
        return Lists.newArrayList(FooResource.class, FooResourceDisabled.class,
            FooResourceDisabledMethodEnabled.class, FooResourceEnabled.class);
    }

    private RequestEvent waitForOkRequest(String path) throws InterruptedException, ExecutionException {
        assertEquals(Response.Status.OK.getStatusCode(), req(path).getStatusCode());
        return getEvent();
    }

    @Path("foo")
    public static class FooResource {
        @GET
        public String foo() {
            return "";
        }
    }

    @Path("fooResourceDisabled")
    @ResourceMetrics(timer = false)
    public static class FooResourceDisabled {
        @GET
        public String foo() {
            return "";
        }
    }

    @Path("fooResourceDisabledMethodEnabled")
    @ResourceMetrics(timer = false)
    public static class FooResourceDisabledMethodEnabled {

        @GET
        @ResourceMetrics(timer = true)
        public String foo() {
            return "";
        }
    }

    @Path("fooResourceEnabled")
    @ResourceMetrics
    public static class FooResourceEnabled {
        @GET
        public String foo() {
            return "";
        }
    }
}
