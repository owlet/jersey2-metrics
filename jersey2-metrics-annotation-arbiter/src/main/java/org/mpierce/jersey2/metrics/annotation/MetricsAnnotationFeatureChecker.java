package org.mpierce.jersey2.metrics.annotation;

/**
 * Abstraction around checking whether a particular feature is enabled on @ResourceMetrics.
 */
interface MetricsAnnotationFeatureChecker {

    /**
     * @param ann annotation to check
     * @return true if the feature relevant to the implementor is enabled
     */
    boolean check(ResourceMetrics ann);
}
